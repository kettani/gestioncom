<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Produitpakages'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="produitpakages form large-9 medium-8 columns content">
    <?= $this->Form->create($produitpakage) ?>
    <fieldset>
        <legend><?= __('Add Produitpakage') ?></legend>
        <?php
            echo $this->Form->input('quantite');
            echo $this->Form->input('parentProduc');
            echo $this->Form->input('article_id', ['options' => $articles]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
