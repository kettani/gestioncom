<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Produitpakage'), ['action' => 'edit', $produitpakage->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Produitpakage'), ['action' => 'delete', $produitpakage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $produitpakage->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Produitpakages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Produitpakage'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="produitpakages view large-9 medium-8 columns content">
    <h3><?= h($produitpakage->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('ParentProduc') ?></th>
            <td><?= h($produitpakage->parentProduc) ?></td>
        </tr>
        <tr>
            <th><?= __('Article') ?></th>
            <td><?= $produitpakage->has('article') ? $this->Html->link($produitpakage->article->id, ['controller' => 'Articles', 'action' => 'view', $produitpakage->article->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($produitpakage->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantite') ?></th>
            <td><?= $this->Number->format($produitpakage->quantite) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($produitpakage->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($produitpakage->modified) ?></td>
        </tr>
    </table>
</div>
