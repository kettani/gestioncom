<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Produitpakage'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="produitpakages index large-9 medium-8 columns content">
    <h3><?= __('Produitpakages') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('quantite') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('parentProduc') ?></th>
                <th><?= $this->Paginator->sort('article_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($produitpakages as $produitpakage): ?>
            <tr>
                <td><?= $this->Number->format($produitpakage->id) ?></td>
                <td><?= $this->Number->format($produitpakage->quantite) ?></td>
                <td><?= h($produitpakage->created) ?></td>
                <td><?= h($produitpakage->modified) ?></td>
                <td><?= h($produitpakage->parentProduc) ?></td>
                <td><?= $produitpakage->has('article') ? $this->Html->link($produitpakage->article->id, ['controller' => 'Articles', 'action' => 'view', $produitpakage->article->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $produitpakage->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $produitpakage->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $produitpakage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $produitpakage->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
