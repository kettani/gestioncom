<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sousfamillesarticle'), ['action' => 'edit', $sousfamillesarticle->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sousfamillesarticle'), ['action' => 'delete', $sousfamillesarticle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sousfamillesarticle->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sousfamillesarticles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sousfamillesarticle'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Famillesarticles'), ['controller' => 'Famillesarticles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Famillesarticle'), ['controller' => 'Famillesarticles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sousfamillesarticles view large-9 medium-8 columns content">
    <h3><?= h($sousfamillesarticle->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Libelle') ?></th>
            <td><?= h($sousfamillesarticle->libelle) ?></td>
        </tr>
        <tr>
            <th><?= __('Famillesarticle') ?></th>
            <td><?= $sousfamillesarticle->has('famillesarticle') ? $this->Html->link($sousfamillesarticle->famillesarticle->id, ['controller' => 'Famillesarticles', 'action' => 'view', $sousfamillesarticle->famillesarticle->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($sousfamillesarticle->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Articles') ?></h4>
        <?php if (!empty($sousfamillesarticle->articles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Libelle') ?></th>
                <th><?= __('Libellecourte') ?></th>
                <th><?= __('Bloque') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Numero Lot Serie') ?></th>
                <th><?= __('Etatvente') ?></th>
                <th><?= __('Etatachat') ?></th>
                <th><?= __('Poids') ?></th>
                <th><?= __('Surface') ?></th>
                <th><?= __('Volume') ?></th>
                <th><?= __('Longueur') ?></th>
                <th><?= __('Epaisseur') ?></th>
                <th><?= __('Hauteure') ?></th>
                <th><?= __('Gamme Id') ?></th>
                <th><?= __('Marquearticle Id') ?></th>
                <th><?= __('Categorie Id') ?></th>
                <th><?= __('Sousfamillesarticle Id') ?></th>
                <th><?= __('Unitegestion Id') ?></th>
                <th><?= __('Typedenomenclature') ?></th>
                <th><?= __('Garantie') ?></th>
                <th><?= __('Delaidelivraison') ?></th>
                <th><?= __('Poidsnet') ?></th>
                <th><?= __('Poidsbrut') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Code Barres') ?></th>
                <th><?= __('Conditionnement') ?></th>
                <th><?= __('Modedesuividestock') ?></th>
                <th><?= __('Tva Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($sousfamillesarticle->articles as $articles): ?>
            <tr>
                <td><?= h($articles->id) ?></td>
                <td><?= h($articles->libelle) ?></td>
                <td><?= h($articles->libellecourte) ?></td>
                <td><?= h($articles->bloque) ?></td>
                <td><?= h($articles->created) ?></td>
                <td><?= h($articles->numero_lot_serie) ?></td>
                <td><?= h($articles->etatvente) ?></td>
                <td><?= h($articles->etatachat) ?></td>
                <td><?= h($articles->poids) ?></td>
                <td><?= h($articles->surface) ?></td>
                <td><?= h($articles->volume) ?></td>
                <td><?= h($articles->Longueur) ?></td>
                <td><?= h($articles->epaisseur) ?></td>
                <td><?= h($articles->hauteure) ?></td>
                <td><?= h($articles->gamme_id) ?></td>
                <td><?= h($articles->marquearticle_id) ?></td>
                <td><?= h($articles->categorie_id) ?></td>
                <td><?= h($articles->sousfamillesarticle_id) ?></td>
                <td><?= h($articles->unitegestion_id) ?></td>
                <td><?= h($articles->typedenomenclature) ?></td>
                <td><?= h($articles->garantie) ?></td>
                <td><?= h($articles->delaidelivraison) ?></td>
                <td><?= h($articles->poidsnet) ?></td>
                <td><?= h($articles->Poidsbrut) ?></td>
                <td><?= h($articles->modified) ?></td>
                <td><?= h($articles->code_barres) ?></td>
                <td><?= h($articles->conditionnement) ?></td>
                <td><?= h($articles->modedesuividestock) ?></td>
                <td><?= h($articles->tva_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Articles', 'action' => 'view', $articles->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Articles', 'action' => 'edit', $articles->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Articles', 'action' => 'delete', $articles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articles->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
