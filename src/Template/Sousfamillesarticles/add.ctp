<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sousfamillesarticles'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Famillesarticles'), ['controller' => 'Famillesarticles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Famillesarticle'), ['controller' => 'Famillesarticles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sousfamillesarticles form large-9 medium-8 columns content">
    <?= $this->Form->create($sousfamillesarticle) ?>
    <fieldset>
        <legend><?= __('Add Sousfamillesarticle') ?></legend>
        <?php
            echo $this->Form->input('libelle');
            echo $this->Form->input('famillesarticles_id', ['options' => $famillesarticles]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
