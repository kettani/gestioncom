<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sousfamillesarticle'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Famillesarticles'), ['controller' => 'Famillesarticles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Famillesarticle'), ['controller' => 'Famillesarticles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sousfamillesarticles index large-9 medium-8 columns content">
    <h3><?= __('Sousfamillesarticles') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('libelle') ?></th>
                <th><?= $this->Paginator->sort('famillesarticles_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sousfamillesarticles as $sousfamillesarticle): ?>
            <tr>
                <td><?= $this->Number->format($sousfamillesarticle->id) ?></td>
                <td><?= h($sousfamillesarticle->libelle) ?></td>
                <td><?= $sousfamillesarticle->has('famillesarticle') ? $this->Html->link($sousfamillesarticle->famillesarticle->id, ['controller' => 'Famillesarticles', 'action' => 'view', $sousfamillesarticle->famillesarticle->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sousfamillesarticle->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sousfamillesarticle->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sousfamillesarticle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sousfamillesarticle->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
