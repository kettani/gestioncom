<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Article'), ['action' => 'edit', $article->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Article'), ['action' => 'delete', $article->id], ['confirm' => __('Are you sure you want to delete # {0}?', $article->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Gammes'), ['controller' => 'Gammes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gamme'), ['controller' => 'Gammes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Marquearticles'), ['controller' => 'Marquearticles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Marquearticle'), ['controller' => 'Marquearticles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sousfamillesarticles'), ['controller' => 'Sousfamillesarticles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sousfamillesarticle'), ['controller' => 'Sousfamillesarticles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Unitegestions'), ['controller' => 'Unitegestions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Unitegestion'), ['controller' => 'Unitegestions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tvas'), ['controller' => 'Tvas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tva'), ['controller' => 'Tvas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articledesubstitutions'), ['controller' => 'Articledesubstitutions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Articledesubstitution'), ['controller' => 'Articledesubstitutions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Produitpakages'), ['controller' => 'Produitpakages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Produitpakage'), ['controller' => 'Produitpakages', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="articles view large-9 medium-8 columns content">
    <h3><?= h($article->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Libelle') ?></th>
            <td><?= h($article->libelle) ?></td>
        </tr>
        <tr>
            <th><?= __('Libellecourte') ?></th>
            <td><?= h($article->libellecourte) ?></td>
        </tr>
        <tr>
            <th><?= __('Bloque') ?></th>
            <td><?= h($article->bloque) ?></td>
        </tr>
        <tr>
            <th><?= __('Numero Lot Serie') ?></th>
            <td><?= h($article->numero_lot_serie) ?></td>
        </tr>
        <tr>
            <th><?= __('Etatvente') ?></th>
            <td><?= h($article->etatvente) ?></td>
        </tr>
        <tr>
            <th><?= __('Etatachat') ?></th>
            <td><?= h($article->etatachat) ?></td>
        </tr>
        <tr>
            <th><?= __('Gamme') ?></th>
            <td><?= $article->has('gamme') ? $this->Html->link($article->gamme->id, ['controller' => 'Gammes', 'action' => 'view', $article->gamme->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Marquearticle') ?></th>
            <td><?= $article->has('marquearticle') ? $this->Html->link($article->marquearticle->id, ['controller' => 'Marquearticles', 'action' => 'view', $article->marquearticle->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Category') ?></th>
            <td><?= $article->has('category') ? $this->Html->link($article->category->id, ['controller' => 'Categories', 'action' => 'view', $article->category->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Sousfamillesarticle') ?></th>
            <td><?= $article->has('sousfamillesarticle') ? $this->Html->link($article->sousfamillesarticle->id, ['controller' => 'Sousfamillesarticles', 'action' => 'view', $article->sousfamillesarticle->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Unitegestion') ?></th>
            <td><?= $article->has('unitegestion') ? $this->Html->link($article->unitegestion->id, ['controller' => 'Unitegestions', 'action' => 'view', $article->unitegestion->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Typedenomenclature') ?></th>
            <td><?= h($article->typedenomenclature) ?></td>
        </tr>
        <tr>
            <th><?= __('Delaidelivraison') ?></th>
            <td><?= h($article->delaidelivraison) ?></td>
        </tr>
        <tr>
            <th><?= __('Code Barres') ?></th>
            <td><?= h($article->code_barres) ?></td>
        </tr>
        <tr>
            <th><?= __('Conditionnement') ?></th>
            <td><?= h($article->conditionnement) ?></td>
        </tr>
        <tr>
            <th><?= __('Modedesuividestock') ?></th>
            <td><?= h($article->modedesuividestock) ?></td>
        </tr>
        <tr>
            <th><?= __('Tva') ?></th>
            <td><?= $article->has('tva') ? $this->Html->link($article->tva->id, ['controller' => 'Tvas', 'action' => 'view', $article->tva->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($article->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Poids') ?></th>
            <td><?= $this->Number->format($article->poids) ?></td>
        </tr>
        <tr>
            <th><?= __('Surface') ?></th>
            <td><?= $this->Number->format($article->surface) ?></td>
        </tr>
        <tr>
            <th><?= __('Volume') ?></th>
            <td><?= $this->Number->format($article->volume) ?></td>
        </tr>
        <tr>
            <th><?= __('Longueur') ?></th>
            <td><?= $this->Number->format($article->Longueur) ?></td>
        </tr>
        <tr>
            <th><?= __('Epaisseur') ?></th>
            <td><?= $this->Number->format($article->epaisseur) ?></td>
        </tr>
        <tr>
            <th><?= __('Hauteure') ?></th>
            <td><?= $this->Number->format($article->hauteure) ?></td>
        </tr>
        <tr>
            <th><?= __('Garantie') ?></th>
            <td><?= $this->Number->format($article->garantie) ?></td>
        </tr>
        <tr>
            <th><?= __('Poidsnet') ?></th>
            <td><?= $this->Number->format($article->poidsnet) ?></td>
        </tr>
        <tr>
            <th><?= __('Poidsbrut') ?></th>
            <td><?= $this->Number->format($article->Poidsbrut) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($article->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($article->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Articledesubstitutions') ?></h4>
        <?php if (!empty($article->articledesubstitutions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('ParentProduc') ?></th>
                <th><?= __('Article Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($article->articledesubstitutions as $articledesubstitutions): ?>
            <tr>
                <td><?= h($articledesubstitutions->id) ?></td>
                <td><?= h($articledesubstitutions->created) ?></td>
                <td><?= h($articledesubstitutions->modified) ?></td>
                <td><?= h($articledesubstitutions->parentProduc) ?></td>
                <td><?= h($articledesubstitutions->article_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Articledesubstitutions', 'action' => 'view', $articledesubstitutions->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Articledesubstitutions', 'action' => 'edit', $articledesubstitutions->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Articledesubstitutions', 'action' => 'delete', $articledesubstitutions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articledesubstitutions->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Produitpakages') ?></h4>
        <?php if (!empty($article->produitpakages)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Quantite') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('ParentProduc') ?></th>
                <th><?= __('Article Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($article->produitpakages as $produitpakages): ?>
            <tr>
                <td><?= h($produitpakages->id) ?></td>
                <td><?= h($produitpakages->quantite) ?></td>
                <td><?= h($produitpakages->created) ?></td>
                <td><?= h($produitpakages->modified) ?></td>
                <td><?= h($produitpakages->parentProduc) ?></td>
                <td><?= h($produitpakages->article_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Produitpakages', 'action' => 'view', $produitpakages->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Produitpakages', 'action' => 'edit', $produitpakages->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Produitpakages', 'action' => 'delete', $produitpakages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $produitpakages->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
