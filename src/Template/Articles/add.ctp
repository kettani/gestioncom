<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Gammes'), ['controller' => 'Gammes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Gamme'), ['controller' => 'Gammes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Marquearticles'), ['controller' => 'Marquearticles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Marquearticle'), ['controller' => 'Marquearticles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sousfamillesarticles'), ['controller' => 'Sousfamillesarticles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sousfamillesarticle'), ['controller' => 'Sousfamillesarticles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Unitegestions'), ['controller' => 'Unitegestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Unitegestion'), ['controller' => 'Unitegestions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tvas'), ['controller' => 'Tvas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tva'), ['controller' => 'Tvas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articledesubstitutions'), ['controller' => 'Articledesubstitutions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Articledesubstitution'), ['controller' => 'Articledesubstitutions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Produitpakages'), ['controller' => 'Produitpakages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Produitpakage'), ['controller' => 'Produitpakages', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="articles form large-9 medium-8 columns content">
    <?= $this->Form->create($article) ?>
    <fieldset>
        <legend><?= __('Add Article') ?></legend>
        <?php
            echo $this->Form->input('libelle');
            echo $this->Form->input('libellecourte');
            echo $this->Form->input('bloque');
            echo $this->Form->input('numero_lot_serie');
            echo $this->Form->input('etatvente');
            echo $this->Form->input('etatachat');
            echo $this->Form->input('poids');
            echo $this->Form->input('surface');
            echo $this->Form->input('volume');
            echo $this->Form->input('Longueur');
            echo $this->Form->input('epaisseur');
            echo $this->Form->input('hauteure');
            echo $this->Form->input('gamme_id', ['options' => $gammes]);
            echo $this->Form->input('marquearticle_id', ['options' => $marquearticles]);
            echo $this->Form->input('categorie_id', ['options' => $categories]);
            echo $this->Form->input('sousfamillesarticle_id', ['options' => $sousfamillesarticles]);
            echo $this->Form->input('unitegestion_id', ['options' => $unitegestions]);
            echo $this->Form->input('typedenomenclature');
            echo $this->Form->input('garantie');
            echo $this->Form->input('delaidelivraison');
            echo $this->Form->input('poidsnet');
            echo $this->Form->input('Poidsbrut');
            echo $this->Form->input('code_barres');
            echo $this->Form->input('conditionnement');
            echo $this->Form->input('modedesuividestock');
            echo $this->Form->input('tva_id', ['options' => $tvas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
