<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Article'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Gammes'), ['controller' => 'Gammes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Gamme'), ['controller' => 'Gammes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Marquearticles'), ['controller' => 'Marquearticles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Marquearticle'), ['controller' => 'Marquearticles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sousfamillesarticles'), ['controller' => 'Sousfamillesarticles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sousfamillesarticle'), ['controller' => 'Sousfamillesarticles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Unitegestions'), ['controller' => 'Unitegestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Unitegestion'), ['controller' => 'Unitegestions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tvas'), ['controller' => 'Tvas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tva'), ['controller' => 'Tvas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articledesubstitutions'), ['controller' => 'Articledesubstitutions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Articledesubstitution'), ['controller' => 'Articledesubstitutions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Produitpakages'), ['controller' => 'Produitpakages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Produitpakage'), ['controller' => 'Produitpakages', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="articles index large-9 medium-8 columns content">
    <h3><?= __('Articles') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('libelle') ?></th>
                <th><?= $this->Paginator->sort('libellecourte') ?></th>
                <th><?= $this->Paginator->sort('bloque') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('numero_lot_serie') ?></th>
                <th><?= $this->Paginator->sort('etatvente') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($articles as $article): ?>
            <tr>
                <td><?= $this->Number->format($article->id) ?></td>
                <td><?= h($article->libelle) ?></td>
                <td><?= h($article->libellecourte) ?></td>
                <td><?= h($article->bloque) ?></td>
                <td><?= h($article->created) ?></td>
                <td><?= h($article->numero_lot_serie) ?></td>
                <td><?= h($article->etatvente) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $article->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $article->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $article->id], ['confirm' => __('Are you sure you want to delete # {0}?', $article->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
