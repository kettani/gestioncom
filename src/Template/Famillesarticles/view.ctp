<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Famillesarticle'), ['action' => 'edit', $famillesarticle->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Famillesarticle'), ['action' => 'delete', $famillesarticle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $famillesarticle->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Famillesarticles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Famillesarticle'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="famillesarticles view large-9 medium-8 columns content">
    <h3><?= h($famillesarticle->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Libelle') ?></th>
            <td><?= h($famillesarticle->libelle) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($famillesarticle->id) ?></td>
        </tr>
    </table>
</div>
