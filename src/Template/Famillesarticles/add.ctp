<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Famillesarticles'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="famillesarticles form large-9 medium-8 columns content">
    <?= $this->Form->create($famillesarticle) ?>
    <fieldset>
        <legend><?= __('Add Famillesarticle') ?></legend>
        <?php
            echo $this->Form->input('libelle');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
