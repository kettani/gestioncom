<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $famillesarticle->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $famillesarticle->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Famillesarticles'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="famillesarticles form large-9 medium-8 columns content">
    <?= $this->Form->create($famillesarticle) ?>
    <fieldset>
        <legend><?= __('Edit Famillesarticle') ?></legend>
        <?php
            echo $this->Form->input('libelle');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
