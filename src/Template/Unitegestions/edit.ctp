<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $unitegestion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $unitegestion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Unitegestions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="unitegestions form large-9 medium-8 columns content">
    <?= $this->Form->create($unitegestion) ?>
    <fieldset>
        <legend><?= __('Edit Unitegestion') ?></legend>
        <?php
            echo $this->Form->input('libelle');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
