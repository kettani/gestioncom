<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bibliothequesDepot->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bibliothequesDepot->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bibliotheques Depots'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bibliothequesDepots form large-9 medium-8 columns content">
    <?= $this->Form->create($bibliothequesDepot) ?>
    <fieldset>
        <legend><?= __('Edit Bibliotheques Depot') ?></legend>
        <?php
            echo $this->Form->input('bibliotheque_id', ['options' => $articles]);
            echo $this->Form->input('depot_id', ['options' => $depots]);
            echo $this->Form->input('quantite');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
