<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Bibliotheques Depot'), ['action' => 'edit', $bibliothequesDepot->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bibliotheques Depot'), ['action' => 'delete', $bibliothequesDepot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bibliothequesDepot->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Bibliotheques Depots'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bibliotheques Depot'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bibliothequesDepots view large-9 medium-8 columns content">
    <h3><?= h($bibliothequesDepot->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Article') ?></th>
            <td><?= $bibliothequesDepot->has('article') ? $this->Html->link($bibliothequesDepot->article->id, ['controller' => 'Articles', 'action' => 'view', $bibliothequesDepot->article->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $bibliothequesDepot->has('depot') ? $this->Html->link($bibliothequesDepot->depot->id, ['controller' => 'Depots', 'action' => 'view', $bibliothequesDepot->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($bibliothequesDepot->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantite') ?></th>
            <td><?= $this->Number->format($bibliothequesDepot->quantite) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($bibliothequesDepot->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($bibliothequesDepot->modified) ?></td>
        </tr>
    </table>
</div>
