<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Bibliotheques Depot'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bibliothequesDepots index large-9 medium-8 columns content">
    <h3><?= __('Bibliotheques Depots') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('bibliotheque_id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('quantite') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bibliothequesDepots as $bibliothequesDepot): ?>
            <tr>
                <td><?= $this->Number->format($bibliothequesDepot->id) ?></td>
                <td><?= $bibliothequesDepot->has('article') ? $this->Html->link($bibliothequesDepot->article->id, ['controller' => 'Articles', 'action' => 'view', $bibliothequesDepot->article->id]) : '' ?></td>
                <td><?= $bibliothequesDepot->has('depot') ? $this->Html->link($bibliothequesDepot->depot->id, ['controller' => 'Depots', 'action' => 'view', $bibliothequesDepot->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($bibliothequesDepot->quantite) ?></td>
                <td><?= h($bibliothequesDepot->created) ?></td>
                <td><?= h($bibliothequesDepot->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $bibliothequesDepot->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bibliothequesDepot->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bibliothequesDepot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bibliothequesDepot->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
