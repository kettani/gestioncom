<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Depot'), ['action' => 'edit', $depot->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Depot'), ['action' => 'delete', $depot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $depot->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="depots view large-9 medium-8 columns content">
    <h3><?= h($depot->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Libelle') ?></th>
            <td><?= h($depot->libelle) ?></td>
        </tr>
        <tr>
            <th><?= __('Adresse') ?></th>
            <td><?= h($depot->adresse) ?></td>
        </tr>
        <tr>
            <th><?= __('Telephone') ?></th>
            <td><?= h($depot->telephone) ?></td>
        </tr>
        <tr>
            <th><?= __('Fax') ?></th>
            <td><?= h($depot->fax) ?></td>
        </tr>
        <tr>
            <th><?= __('Contact') ?></th>
            <td><?= h($depot->contact) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($depot->id) ?></td>
        </tr>
    </table>
</div>
