<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="depots form large-9 medium-8 columns content">
    <?= $this->Form->create($depot) ?>
    <fieldset>
        <legend><?= __('Add Depot') ?></legend>
        <?php
            echo $this->Form->input('libelle');
            echo $this->Form->input('adresse');
            echo $this->Form->input('telephone');
            echo $this->Form->input('fax');
            echo $this->Form->input('contact');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
