<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="depots index large-9 medium-8 columns content">
    <h3><?= __('Depots') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('libelle') ?></th>
                <th><?= $this->Paginator->sort('adresse') ?></th>
                <th><?= $this->Paginator->sort('telephone') ?></th>
                <th><?= $this->Paginator->sort('fax') ?></th>
                <th><?= $this->Paginator->sort('contact') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($depots as $depot): ?>
            <tr>
                <td><?= $this->Number->format($depot->id) ?></td>
                <td><?= h($depot->libelle) ?></td>
                <td><?= h($depot->adresse) ?></td>
                <td><?= h($depot->telephone) ?></td>
                <td><?= h($depot->fax) ?></td>
                <td><?= h($depot->contact) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $depot->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $depot->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $depot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $depot->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
