<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tva->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tva->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tvas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tvas form large-9 medium-8 columns content">
    <?= $this->Form->create($tva) ?>
    <fieldset>
        <legend><?= __('Edit Tva') ?></legend>
        <?php
            echo $this->Form->input('taux');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
