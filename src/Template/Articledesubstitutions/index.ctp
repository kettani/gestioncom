<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Articledesubstitution'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="articledesubstitutions index large-9 medium-8 columns content">
    <h3><?= __('Articledesubstitutions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('parentProduc') ?></th>
                <th><?= $this->Paginator->sort('article_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($articledesubstitutions as $articledesubstitution): ?>
            <tr>
                <td><?= $this->Number->format($articledesubstitution->id) ?></td>
                <td><?= h($articledesubstitution->created) ?></td>
                <td><?= h($articledesubstitution->modified) ?></td>
                <td><?= h($articledesubstitution->parentProduc) ?></td>
                <td><?= $articledesubstitution->has('article') ? $this->Html->link($articledesubstitution->article->id, ['controller' => 'Articles', 'action' => 'view', $articledesubstitution->article->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $articledesubstitution->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $articledesubstitution->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $articledesubstitution->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articledesubstitution->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
