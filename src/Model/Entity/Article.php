<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Article Entity.
 *
 * @property int $id
 * @property string $libelle
 * @property string $libellecourte
 * @property string $bloque
 * @property \Cake\I18n\Time $created
 * @property string $numero_lot_serie
 * @property string $etatvente
 * @property string $etatachat
 * @property float $poids
 * @property float $surface
 * @property float $volume
 * @property float $Longueur
 * @property float $epaisseur
 * @property float $hauteure
 * @property int $gamme_id
 * @property \App\Model\Entity\Gamme $gamme
 * @property int $marquearticle_id
 * @property \App\Model\Entity\Marquearticle $marquearticle
 * @property int $categorie_id
 * @property \App\Model\Entity\Category $category
 * @property int $sousfamillesarticle_id
 * @property \App\Model\Entity\Sousfamillesarticle $sousfamillesarticle
 * @property int $unitegestion_id
 * @property \App\Model\Entity\Unitegestion $unitegestion
 * @property string $typedenomenclature
 * @property int $garantie
 * @property string $delaidelivraison
 * @property float $poidsnet
 * @property float $Poidsbrut
 * @property \Cake\I18n\Time $modified
 * @property string $code_barres
 * @property string $conditionnement
 * @property string $modedesuividestock
 * @property int $tva_id
 * @property \App\Model\Entity\Tva $tva
 * @property \App\Model\Entity\Articledesubstitution[] $articledesubstitutions
 * @property \App\Model\Entity\Produitpakage[] $produitpakages
 */
class Article extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
