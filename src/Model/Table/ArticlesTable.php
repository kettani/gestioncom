<?php
namespace App\Model\Table;

use App\Model\Entity\Article;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Gammes
 * @property \Cake\ORM\Association\BelongsTo $Marquearticles
 * @property \Cake\ORM\Association\BelongsTo $Categories
 * @property \Cake\ORM\Association\BelongsTo $Sousfamillesarticles
 * @property \Cake\ORM\Association\BelongsTo $Unitegestions
 * @property \Cake\ORM\Association\BelongsTo $Tvas
 * @property \Cake\ORM\Association\HasMany $Articledesubstitutions
 * @property \Cake\ORM\Association\HasMany $Produitpakages
 */
class ArticlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('articles');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Gammes', [
            'foreignKey' => 'gamme_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Marquearticles', [
            'foreignKey' => 'marquearticle_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'categorie_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Sousfamillesarticles', [
            'foreignKey' => 'sousfamillesarticle_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Unitegestions', [
            'foreignKey' => 'unitegestion_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tvas', [
            'foreignKey' => 'tva_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Articledesubstitutions', [
            'foreignKey' => 'article_id'
        ]);
        $this->hasMany('Produitpakages', [
            'foreignKey' => 'article_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('libelle');

        $validator
            ->allowEmpty('libellecourte');

        $validator
            ->allowEmpty('bloque');

        $validator
            ->allowEmpty('numero_lot_serie');

        $validator
            ->allowEmpty('etatvente');

        $validator
            ->allowEmpty('etatachat');

        $validator
            ->add('poids', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('poids');

        $validator
            ->add('surface', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('surface');

        $validator
            ->add('volume', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('volume');

        $validator
            ->add('Longueur', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('Longueur');

        $validator
            ->add('epaisseur', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('epaisseur');

        $validator
            ->add('hauteure', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('hauteure');

        $validator
            ->allowEmpty('typedenomenclature');

        $validator
            ->add('garantie', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('garantie');

        $validator
            ->allowEmpty('delaidelivraison');

        $validator
            ->add('poidsnet', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('poidsnet');

        $validator
            ->add('Poidsbrut', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('Poidsbrut');

        $validator
            ->allowEmpty('code_barres');

        $validator
            ->allowEmpty('conditionnement');

        $validator
            ->allowEmpty('modedesuividestock');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['gamme_id'], 'Gammes'));
        $rules->add($rules->existsIn(['marquearticle_id'], 'Marquearticles'));
        $rules->add($rules->existsIn(['categorie_id'], 'Categories'));
        $rules->add($rules->existsIn(['sousfamillesarticle_id'], 'Sousfamillesarticles'));
        $rules->add($rules->existsIn(['unitegestion_id'], 'Unitegestions'));
        $rules->add($rules->existsIn(['tva_id'], 'Tvas'));
        return $rules;
    }
}
