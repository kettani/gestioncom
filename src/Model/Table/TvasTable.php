<?php
namespace App\Model\Table;

use App\Model\Entity\Tva;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tvas Model
 *
 * @property \Cake\ORM\Association\HasMany $Articles
 */
class TvasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tvas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Articles', [
            'foreignKey' => 'tva_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('taux', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('taux');

        return $validator;
    }
}
