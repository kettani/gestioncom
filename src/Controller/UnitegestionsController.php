<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Unitegestions Controller
 *
 * @property \App\Model\Table\UnitegestionsTable $Unitegestions
 */
class UnitegestionsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('unitegestions', $this->paginate($this->Unitegestions));
        $this->set('_serialize', ['unitegestions']);
    }

    /**
     * View method
     *
     * @param string|null $id Unitegestion id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $unitegestion = $this->Unitegestions->get($id, [
            'contain' => ['Articles']
        ]);
        $this->set('unitegestion', $unitegestion);
        $this->set('_serialize', ['unitegestion']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $unitegestion = $this->Unitegestions->newEntity();
        if ($this->request->is('post')) {
            $unitegestion = $this->Unitegestions->patchEntity($unitegestion, $this->request->data);
            if ($this->Unitegestions->save($unitegestion)) {
                $this->Flash->success(__('The unitegestion has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The unitegestion could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('unitegestion'));
        $this->set('_serialize', ['unitegestion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Unitegestion id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $unitegestion = $this->Unitegestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $unitegestion = $this->Unitegestions->patchEntity($unitegestion, $this->request->data);
            if ($this->Unitegestions->save($unitegestion)) {
                $this->Flash->success(__('The unitegestion has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The unitegestion could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('unitegestion'));
        $this->set('_serialize', ['unitegestion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Unitegestion id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $unitegestion = $this->Unitegestions->get($id);
        if ($this->Unitegestions->delete($unitegestion)) {
            $this->Flash->success(__('The unitegestion has been deleted.'));
        } else {
            $this->Flash->error(__('The unitegestion could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
