<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Depots Controller
 *
 * @property \App\Model\Table\DepotsTable $Depots
 */
class DepotsController extends AppController
{

    /**
     * methode principale
     *
     * @return void
     */
    public function index()
    {
        $this->set('depots', $this->paginate($this->Depots));
        $this->set('_serialize', ['depots']);
    }

    /**
     * View method
     *
     * @param string|null $id Depot id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $depot = $this->Depots->get($id, [
            'contain' => []
        ]);
        $this->set('depot', $depot);
        $this->set('_serialize', ['depot']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.(ajout)
     */
    public function add()
    {
        $depot = $this->Depots->newEntity();
        if ($this->request->is('post')) {
            $depot = $this->Depots->patchEntity($depot, $this->request->data);
            if ($this->Depots->save($depot)) {
                $this->Flash->success(__('The depot has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The depot could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('depot'));
        $this->set('_serialize', ['depot']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Depot id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $depot = $this->Depots->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $depot = $this->Depots->patchEntity($depot, $this->request->data);
            if ($this->Depots->save($depot)) {
                $this->Flash->success(__('The depot has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The depot could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('depot'));
        $this->set('_serialize', ['depot']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Depot id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $depot = $this->Depots->get($id);
        if ($this->Depots->delete($depot)) {
            $this->Flash->success(__('The depot has been deleted.'));
        } else {
            $this->Flash->error(__('The depot could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
