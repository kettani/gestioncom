<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Articledesubstitutions Controller
 *
 * @property \App\Model\Table\ArticledesubstitutionsTable $Articledesubstitutions
 */
class ArticledesubstitutionsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Articles']
        ];
        $this->set('articledesubstitutions', $this->paginate($this->Articledesubstitutions));
        $this->set('_serialize', ['articledesubstitutions']);
    }

    /**
     * View method
     *
     * @param string|null $id Articledesubstitution id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $articledesubstitution = $this->Articledesubstitutions->get($id, [
            'contain' => ['Articles']
        ]);
        $this->set('articledesubstitution', $articledesubstitution);
        $this->set('_serialize', ['articledesubstitution']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $articledesubstitution = $this->Articledesubstitutions->newEntity();
        if ($this->request->is('post')) {
            $articledesubstitution = $this->Articledesubstitutions->patchEntity($articledesubstitution, $this->request->data);
            if ($this->Articledesubstitutions->save($articledesubstitution)) {
                $this->Flash->success(__('The articledesubstitution has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The articledesubstitution could not be saved. Please, try again.'));
            }
        }
        $articles = $this->Articledesubstitutions->Articles->find('list', ['limit' => 200]);
        $this->set(compact('articledesubstitution', 'articles'));
        $this->set('_serialize', ['articledesubstitution']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Articledesubstitution id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $articledesubstitution = $this->Articledesubstitutions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $articledesubstitution = $this->Articledesubstitutions->patchEntity($articledesubstitution, $this->request->data);
            if ($this->Articledesubstitutions->save($articledesubstitution)) {
                $this->Flash->success(__('The articledesubstitution has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The articledesubstitution could not be saved. Please, try again.'));
            }
        }
        $articles = $this->Articledesubstitutions->Articles->find('list', ['limit' => 200]);
        $this->set(compact('articledesubstitution', 'articles'));
        $this->set('_serialize', ['articledesubstitution']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Articledesubstitution id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $articledesubstitution = $this->Articledesubstitutions->get($id);
        if ($this->Articledesubstitutions->delete($articledesubstitution)) {
            $this->Flash->success(__('The articledesubstitution has been deleted.'));
        } else {
            $this->Flash->error(__('The articledesubstitution could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
