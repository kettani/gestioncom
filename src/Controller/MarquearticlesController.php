<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Marquearticles Controller
 *
 * @property \App\Model\Table\MarquearticlesTable $Marquearticles
 */
class MarquearticlesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('marquearticles', $this->paginate($this->Marquearticles));
        $this->set('_serialize', ['marquearticles']);
    }

    /**
     * View method
     *
     * @param string|null $id Marquearticle id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $marquearticle = $this->Marquearticles->get($id, [
            'contain' => ['Articles']
        ]);
        $this->set('marquearticle', $marquearticle);
        $this->set('_serialize', ['marquearticle']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $marquearticle = $this->Marquearticles->newEntity();
        if ($this->request->is('post')) {
            $marquearticle = $this->Marquearticles->patchEntity($marquearticle, $this->request->data);
            if ($this->Marquearticles->save($marquearticle)) {
                $this->Flash->success(__('The marquearticle has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The marquearticle could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('marquearticle'));
        $this->set('_serialize', ['marquearticle']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Marquearticle id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $marquearticle = $this->Marquearticles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $marquearticle = $this->Marquearticles->patchEntity($marquearticle, $this->request->data);
            if ($this->Marquearticles->save($marquearticle)) {
                $this->Flash->success(__('The marquearticle has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The marquearticle could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('marquearticle'));
        $this->set('_serialize', ['marquearticle']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Marquearticle id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $marquearticle = $this->Marquearticles->get($id);
        if ($this->Marquearticles->delete($marquearticle)) {
            $this->Flash->success(__('The marquearticle has been deleted.'));
        } else {
            $this->Flash->error(__('The marquearticle could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
