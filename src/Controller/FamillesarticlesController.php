<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Famillesarticles Controller
 *
 * @property \App\Model\Table\FamillesarticlesTable $Famillesarticles
 */
class FamillesarticlesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('famillesarticles', $this->paginate($this->Famillesarticles));
        $this->set('_serialize', ['famillesarticles']);
    }

    /**
     * View method
     *
     * @param string|null $id Famillesarticle id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $famillesarticle = $this->Famillesarticles->get($id, [
            'contain' => []
        ]);
        $this->set('famillesarticle', $famillesarticle);
        $this->set('_serialize', ['famillesarticle']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $famillesarticle = $this->Famillesarticles->newEntity();
        if ($this->request->is('post')) {
            $famillesarticle = $this->Famillesarticles->patchEntity($famillesarticle, $this->request->data);
            if ($this->Famillesarticles->save($famillesarticle)) {
                $this->Flash->success(__('The famillesarticle has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The famillesarticle could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('famillesarticle'));
        $this->set('_serialize', ['famillesarticle']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Famillesarticle id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $famillesarticle = $this->Famillesarticles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $famillesarticle = $this->Famillesarticles->patchEntity($famillesarticle, $this->request->data);
            if ($this->Famillesarticles->save($famillesarticle)) {
                $this->Flash->success(__('The famillesarticle has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The famillesarticle could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('famillesarticle'));
        $this->set('_serialize', ['famillesarticle']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Famillesarticle id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $famillesarticle = $this->Famillesarticles->get($id);
        if ($this->Famillesarticles->delete($famillesarticle)) {
            $this->Flash->success(__('The famillesarticle has been deleted.'));
        } else {
            $this->Flash->error(__('The famillesarticle could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
