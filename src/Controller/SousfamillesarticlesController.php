<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Sousfamillesarticles Controller
 *
 * @property \App\Model\Table\SousfamillesarticlesTable $Sousfamillesarticles
 */
class SousfamillesarticlesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Famillesarticles']
        ];
        $this->set('sousfamillesarticles', $this->paginate($this->Sousfamillesarticles));
        $this->set('_serialize', ['sousfamillesarticles']);
    }

    /**
     * View method
     *
     * @param string|null $id Sousfamillesarticle id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sousfamillesarticle = $this->Sousfamillesarticles->get($id, [
            'contain' => ['Famillesarticles', 'Articles']
        ]);
        $this->set('sousfamillesarticle', $sousfamillesarticle);
        $this->set('_serialize', ['sousfamillesarticle']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sousfamillesarticle = $this->Sousfamillesarticles->newEntity();
        if ($this->request->is('post')) {
            $sousfamillesarticle = $this->Sousfamillesarticles->patchEntity($sousfamillesarticle, $this->request->data);
            if ($this->Sousfamillesarticles->save($sousfamillesarticle)) {
                $this->Flash->success(__('The sousfamillesarticle has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sousfamillesarticle could not be saved. Please, try again.'));
            }
        }
        $famillesarticles = $this->Sousfamillesarticles->Famillesarticles->find('list', ['limit' => 200]);
        $this->set(compact('sousfamillesarticle', 'famillesarticles'));
        $this->set('_serialize', ['sousfamillesarticle']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sousfamillesarticle id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sousfamillesarticle = $this->Sousfamillesarticles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sousfamillesarticle = $this->Sousfamillesarticles->patchEntity($sousfamillesarticle, $this->request->data);
            if ($this->Sousfamillesarticles->save($sousfamillesarticle)) {
                $this->Flash->success(__('The sousfamillesarticle has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sousfamillesarticle could not be saved. Please, try again.'));
            }
        }
        $famillesarticles = $this->Sousfamillesarticles->Famillesarticles->find('list', ['limit' => 200]);
        $this->set(compact('sousfamillesarticle', 'famillesarticles'));
        $this->set('_serialize', ['sousfamillesarticle']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sousfamillesarticle id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sousfamillesarticle = $this->Sousfamillesarticles->get($id);
        if ($this->Sousfamillesarticles->delete($sousfamillesarticle)) {
            $this->Flash->success(__('The sousfamillesarticle has been deleted.'));
        } else {
            $this->Flash->error(__('The sousfamillesarticle could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
