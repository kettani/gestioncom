<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Produitpakages Controller
 *
 * @property \App\Model\Table\ProduitpakagesTable $Produitpakages
 */
class ProduitpakagesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Articles']
        ];
        $this->set('produitpakages', $this->paginate($this->Produitpakages));
        $this->set('_serialize', ['produitpakages']);
    }

    /**
     * View method
     *
     * @param string|null $id Produitpakage id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $produitpakage = $this->Produitpakages->get($id, [
            'contain' => ['Articles']
        ]);
        $this->set('produitpakage', $produitpakage);
        $this->set('_serialize', ['produitpakage']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $produitpakage = $this->Produitpakages->newEntity();
        if ($this->request->is('post')) {
            $produitpakage = $this->Produitpakages->patchEntity($produitpakage, $this->request->data);
            if ($this->Produitpakages->save($produitpakage)) {
                $this->Flash->success(__('The produitpakage has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The produitpakage could not be saved. Please, try again.'));
            }
        }
        $articles = $this->Produitpakages->Articles->find('list', ['limit' => 200]);
        $this->set(compact('produitpakage', 'articles'));
        $this->set('_serialize', ['produitpakage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Produitpakage id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $produitpakage = $this->Produitpakages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $produitpakage = $this->Produitpakages->patchEntity($produitpakage, $this->request->data);
            if ($this->Produitpakages->save($produitpakage)) {
                $this->Flash->success(__('The produitpakage has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The produitpakage could not be saved. Please, try again.'));
            }
        }
        $articles = $this->Produitpakages->Articles->find('list', ['limit' => 200]);
        $this->set(compact('produitpakage', 'articles'));
        $this->set('_serialize', ['produitpakage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Produitpakage id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $produitpakage = $this->Produitpakages->get($id);
        if ($this->Produitpakages->delete($produitpakage)) {
            $this->Flash->success(__('The produitpakage has been deleted.'));
        } else {
            $this->Flash->error(__('The produitpakage could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
