<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Gammes Controller
 *
 * @property \App\Model\Table\GammesTable $Gammes
 */
class GammesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('gammes', $this->paginate($this->Gammes));
        $this->set('_serialize', ['gammes']);
    }

    /**
     * View method
     *
     * @param string|null $id Gamme id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gamme = $this->Gammes->get($id, [
            'contain' => ['Articles']
        ]);
        $this->set('gamme', $gamme);
        $this->set('_serialize', ['gamme']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gamme = $this->Gammes->newEntity();
        if ($this->request->is('post')) {
            $gamme = $this->Gammes->patchEntity($gamme, $this->request->data);
            if ($this->Gammes->save($gamme)) {
                $this->Flash->success(__('The gamme has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gamme could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('gamme'));
        $this->set('_serialize', ['gamme']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Gamme id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gamme = $this->Gammes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gamme = $this->Gammes->patchEntity($gamme, $this->request->data);
            if ($this->Gammes->save($gamme)) {
                $this->Flash->success(__('The gamme has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gamme could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('gamme'));
        $this->set('_serialize', ['gamme']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gamme id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gamme = $this->Gammes->get($id);
        if ($this->Gammes->delete($gamme)) {
            $this->Flash->success(__('The gamme has been deleted.'));
        } else {
            $this->Flash->error(__('The gamme could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
