<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BibliothequesDepots Controller
 *
 * @property \App\Model\Table\BibliothequesDepotsTable $BibliothequesDepots
 */
class BibliothequesDepotsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Articles', 'Depots']
        ];
        $this->set('bibliothequesDepots', $this->paginate($this->BibliothequesDepots));
        $this->set('_serialize', ['bibliothequesDepots']);
    }

    /**
     * View method
     *
     * @param string|null $id Bibliotheques Depot id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bibliothequesDepot = $this->BibliothequesDepots->get($id, [
            'contain' => ['Articles', 'Depots']
        ]);
        $this->set('bibliothequesDepot', $bibliothequesDepot);
        $this->set('_serialize', ['bibliothequesDepot']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bibliothequesDepot = $this->BibliothequesDepots->newEntity();
        if ($this->request->is('post')) {
            $bibliothequesDepot = $this->BibliothequesDepots->patchEntity($bibliothequesDepot, $this->request->data);
            if ($this->BibliothequesDepots->save($bibliothequesDepot)) {
                $this->Flash->success(__('The bibliotheques depot has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bibliotheques depot could not be saved. Please, try again.'));
            }
        }
        $articles = $this->BibliothequesDepots->Articles->find('list', ['limit' => 200]);
        $depots = $this->BibliothequesDepots->Depots->find('list', ['limit' => 200]);
        $this->set(compact('bibliothequesDepot', 'articles', 'depots'));
        $this->set('_serialize', ['bibliothequesDepot']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Bibliotheques Depot id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bibliothequesDepot = $this->BibliothequesDepots->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bibliothequesDepot = $this->BibliothequesDepots->patchEntity($bibliothequesDepot, $this->request->data);
            if ($this->BibliothequesDepots->save($bibliothequesDepot)) {
                $this->Flash->success(__('The bibliotheques depot has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bibliotheques depot could not be saved. Please, try again.'));
            }
        }
        $articles = $this->BibliothequesDepots->Articles->find('list', ['limit' => 200]);
        $depots = $this->BibliothequesDepots->Depots->find('list', ['limit' => 200]);
        $this->set(compact('bibliothequesDepot', 'articles', 'depots'));
        $this->set('_serialize', ['bibliothequesDepot']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Bibliotheques Depot id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bibliothequesDepot = $this->BibliothequesDepots->get($id);
        if ($this->BibliothequesDepots->delete($bibliothequesDepot)) {
            $this->Flash->success(__('The bibliotheques depot has been deleted.'));
        } else {
            $this->Flash->error(__('The bibliotheques depot could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
