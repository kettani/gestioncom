<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SousfamillesarticlesFixture
 *
 */
class SousfamillesarticlesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'libelle' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'famillesarticles_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_sousfamillesarticles_famillesarticles1_idx' => ['type' => 'index', 'columns' => ['famillesarticles_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'sousfamillesarticles_id_UNIQUE' => ['type' => 'unique', 'columns' => ['id'], 'length' => []],
            'libelle_UNIQUE' => ['type' => 'unique', 'columns' => ['libelle'], 'length' => []],
            'fk_sousfamillesarticles_famillesarticles1' => ['type' => 'foreign', 'columns' => ['famillesarticles_id'], 'references' => ['famillesarticles', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'libelle' => 'Lorem ipsum dolor sit amet',
            'famillesarticles_id' => 1
        ],
    ];
}
