<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BibliothequesDepotsFixture
 *
 */
class BibliothequesDepotsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'bibliotheque_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'depot_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantite' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_bibliotheques_has_depots_depots1_idx' => ['type' => 'index', 'columns' => ['depot_id'], 'length' => []],
            'fk_bibliotheques_has_depots_bibliotheques1_idx' => ['type' => 'index', 'columns' => ['bibliotheque_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'bibliotheques_id_UNIQUE' => ['type' => 'unique', 'columns' => ['bibliotheque_id'], 'length' => []],
            'depots_id_UNIQUE' => ['type' => 'unique', 'columns' => ['depot_id'], 'length' => []],
            'fk_bibliotheques_has_depots_bibliotheques1' => ['type' => 'foreign', 'columns' => ['bibliotheque_id'], 'references' => ['articles', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_bibliotheques_has_depots_depots1' => ['type' => 'foreign', 'columns' => ['depot_id'], 'references' => ['depots', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'bibliotheque_id' => 1,
            'depot_id' => 1,
            'quantite' => 1,
            'created' => '2015-12-19 14:40:10',
            'modified' => '2015-12-19 14:40:10'
        ],
    ];
}
