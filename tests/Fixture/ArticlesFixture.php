<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArticlesFixture
 *
 */
class ArticlesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'libelle' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'libellecourte' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'bloque' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'numero_lot_serie' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'etatvente' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'etatachat' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'poids' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'surface' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'volume' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'Longueur' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'epaisseur' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'hauteure' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'gamme_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'marquearticle_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'categorie_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sousfamillesarticle_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'unitegestion_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'typedenomenclature' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'garantie' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'delaidelivraison' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'poidsnet' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'Poidsbrut' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'code_barres' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'conditionnement' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'modedesuividestock' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'tva_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_bibliotheques_gammes1_idx' => ['type' => 'index', 'columns' => ['gamme_id'], 'length' => []],
            'fk_bibliotheques_marquearticles1_idx' => ['type' => 'index', 'columns' => ['marquearticle_id'], 'length' => []],
            'fk_bibliotheques_categories1_idx' => ['type' => 'index', 'columns' => ['categorie_id'], 'length' => []],
            'fk_bibliotheques_sousfamillesarticles1_idx' => ['type' => 'index', 'columns' => ['sousfamillesarticle_id'], 'length' => []],
            'fk_bibliotheques_unitegestions1_idx' => ['type' => 'index', 'columns' => ['unitegestion_id'], 'length' => []],
            'fk_bibliotheques_tvas1_idx' => ['type' => 'index', 'columns' => ['tva_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_bibliotheques_categories1' => ['type' => 'foreign', 'columns' => ['categorie_id'], 'references' => ['categories', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_bibliotheques_gammes1' => ['type' => 'foreign', 'columns' => ['gamme_id'], 'references' => ['gammes', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_bibliotheques_marquearticles1' => ['type' => 'foreign', 'columns' => ['marquearticle_id'], 'references' => ['marquearticles', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_bibliotheques_sousfamillesarticles1' => ['type' => 'foreign', 'columns' => ['sousfamillesarticle_id'], 'references' => ['sousfamillesarticles', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_bibliotheques_tvas1' => ['type' => 'foreign', 'columns' => ['tva_id'], 'references' => ['tvas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_bibliotheques_unitegestions1' => ['type' => 'foreign', 'columns' => ['unitegestion_id'], 'references' => ['unitegestions', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'libelle' => 'Lorem ipsum dolor sit amet',
            'libellecourte' => 'Lorem ipsum dolor sit amet',
            'bloque' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-12-19 14:40:09',
            'numero_lot_serie' => 'Lorem ipsum dolor sit amet',
            'etatvente' => 'Lorem ipsum dolor sit amet',
            'etatachat' => 'Lorem ipsum dolor sit amet',
            'poids' => 1,
            'surface' => 1,
            'volume' => 1,
            'Longueur' => 1,
            'epaisseur' => 1,
            'hauteure' => 1,
            'gamme_id' => 1,
            'marquearticle_id' => 1,
            'categorie_id' => 1,
            'sousfamillesarticle_id' => 1,
            'unitegestion_id' => 1,
            'typedenomenclature' => 'Lorem ipsum dolor sit amet',
            'garantie' => 1,
            'delaidelivraison' => 'Lorem ipsum dolor sit amet',
            'poidsnet' => 1,
            'Poidsbrut' => 1,
            'modified' => '2015-12-19 14:40:09',
            'code_barres' => 'Lorem ipsum dolor sit amet',
            'conditionnement' => 'Lorem ipsum dolor sit amet',
            'modedesuividestock' => 'Lorem ipsum dolor sit amet',
            'tva_id' => 1
        ],
    ];
}
