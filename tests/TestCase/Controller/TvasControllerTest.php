<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TvasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TvasController Test Case
 */
class TvasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tvas',
        'app.articles',
        'app.gammes',
        'app.marquearticles',
        'app.categories',
        'app.sousfamillesarticles',
        'app.famillesarticles',
        'app.unitegestions',
        'app.articledesubstitutions',
        'app.produitpakages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
