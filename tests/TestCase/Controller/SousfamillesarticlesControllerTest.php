<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SousfamillesarticlesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SousfamillesarticlesController Test Case
 */
class SousfamillesarticlesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sousfamillesarticles',
        'app.famillesarticles',
        'app.articles',
        'app.gammes',
        'app.marquearticles',
        'app.categories',
        'app.unitegestions',
        'app.tvas',
        'app.articledesubstitutions',
        'app.produitpakages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
