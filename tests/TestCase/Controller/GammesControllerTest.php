<?php
namespace App\Test\TestCase\Controller;

use App\Controller\GammesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\GammesController Test Case
 */
class GammesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.gammes',
        'app.articles',
        'app.marquearticles',
        'app.categories',
        'app.sousfamillesarticles',
        'app.famillesarticles',
        'app.unitegestions',
        'app.tvas',
        'app.articledesubstitutions',
        'app.produitpakages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
