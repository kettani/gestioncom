<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TvasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TvasTable Test Case
 */
class TvasTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tvas',
        'app.articles',
        'app.gammes',
        'app.marquearticles',
        'app.categories',
        'app.sousfamillesarticles',
        'app.famillesarticles',
        'app.unitegestions',
        'app.articledesubstitutions',
        'app.produitpakages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tvas') ? [] : ['className' => 'App\Model\Table\TvasTable'];
        $this->Tvas = TableRegistry::get('Tvas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tvas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
