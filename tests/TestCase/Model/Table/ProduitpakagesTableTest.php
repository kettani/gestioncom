<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProduitpakagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProduitpakagesTable Test Case
 */
class ProduitpakagesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.produitpakages',
        'app.articles',
        'app.gammes',
        'app.marquearticles',
        'app.categories',
        'app.sousfamillesarticles',
        'app.unitegestions',
        'app.tvas',
        'app.articledesubstitutions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Produitpakages') ? [] : ['className' => 'App\Model\Table\ProduitpakagesTable'];
        $this->Produitpakages = TableRegistry::get('Produitpakages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Produitpakages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
