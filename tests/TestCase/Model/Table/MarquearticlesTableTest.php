<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MarquearticlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MarquearticlesTable Test Case
 */
class MarquearticlesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.marquearticles',
        'app.articles',
        'app.gammes',
        'app.categories',
        'app.sousfamillesarticles',
        'app.unitegestions',
        'app.tvas',
        'app.articledesubstitutions',
        'app.produitpakages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Marquearticles') ? [] : ['className' => 'App\Model\Table\MarquearticlesTable'];
        $this->Marquearticles = TableRegistry::get('Marquearticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Marquearticles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
