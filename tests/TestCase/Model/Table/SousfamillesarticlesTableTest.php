<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SousfamillesarticlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SousfamillesarticlesTable Test Case
 */
class SousfamillesarticlesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sousfamillesarticles',
        'app.famillesarticles',
        'app.articles',
        'app.gammes',
        'app.marquearticles',
        'app.categories',
        'app.unitegestions',
        'app.tvas',
        'app.articledesubstitutions',
        'app.produitpakages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sousfamillesarticles') ? [] : ['className' => 'App\Model\Table\SousfamillesarticlesTable'];
        $this->Sousfamillesarticles = TableRegistry::get('Sousfamillesarticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sousfamillesarticles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
