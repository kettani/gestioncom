<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BibliothequesDepotsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BibliothequesDepotsTable Test Case
 */
class BibliothequesDepotsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bibliotheques_depots',
        'app.articles',
        'app.gammes',
        'app.marquearticles',
        'app.categories',
        'app.sousfamillesarticles',
        'app.unitegestions',
        'app.tvas',
        'app.articledesubstitutions',
        'app.produitpakages',
        'app.depots'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BibliothequesDepots') ? [] : ['className' => 'App\Model\Table\BibliothequesDepotsTable'];
        $this->BibliothequesDepots = TableRegistry::get('BibliothequesDepots', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BibliothequesDepots);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
