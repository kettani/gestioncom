<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UnitegestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UnitegestionsTable Test Case
 */
class UnitegestionsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.unitegestions',
        'app.articles',
        'app.gammes',
        'app.marquearticles',
        'app.categories',
        'app.sousfamillesarticles',
        'app.famillesarticles',
        'app.tvas',
        'app.articledesubstitutions',
        'app.produitpakages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Unitegestions') ? [] : ['className' => 'App\Model\Table\UnitegestionsTable'];
        $this->Unitegestions = TableRegistry::get('Unitegestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Unitegestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
